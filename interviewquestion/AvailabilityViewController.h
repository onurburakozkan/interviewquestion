//
//  ViewController.h
//  interviewquestion
//
//  Created by Onur Burak Ozkan on 22/09/14.
//  Copyright (c) 2014 interview. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AvailabilityRequest.h"
#import "AvailabilityResponse.h"
#import "AvailabilityModelCell.h"

@interface AvailabilityViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *availabilityTableView;

@end