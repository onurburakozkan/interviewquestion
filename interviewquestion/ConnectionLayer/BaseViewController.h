//
//  BaseViewController.h
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "NetworkHTTPRequestOperation.h"
#import "NetworkHTTPClient.h"
#import <objc/runtime.h>

@interface BaseViewController : UIViewController<UIGestureRecognizerDelegate,UITextFieldDelegate>

@property (nonatomic, strong, readonly) NSMutableDictionary *responses;
@property (nonatomic, strong) NSMutableDictionary *storeData;
@property (nonatomic, strong) id data;
@property (nonatomic) BOOL preventsTapGesture;
@property(nonatomic, strong) NSString * requestType;

@property (strong, nonatomic) UITextField *activeTextField;

-(void)setNeedsRefreshData;

-(NetworkHTTPRequestOperation*)mainRequest;
-(void)mainRequestDone:(NetworkHTTPRequestOperation *)request;
-(void)mainRequestWithOperation:(NetworkHTTPRequestOperation *)operation WentWrong:(NSError *)error;
-(UIView*)findFirstResponder:(UIView*)parentView;
-(void)removeTapGesture;

@end
