//
//  NetworkHTTPRequestOperation.h
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import "AFHTTPRequestOperation.h"

@interface NetworkHTTPRequestOperation : AFHTTPRequestOperation

@property (readonly, nonatomic, strong) id responseObject;
@property (readwrite, nonatomic, strong) NSMutableURLRequest *requestUrl;
@property (nonatomic, strong) NSString *requestOperationType; // added to handle asynchronious responses
@property (nonatomic, strong) NSNumber *showLoading;

+ (instancetype)BaseRequestOperationWithRequest:(NSMutableURLRequest *)urlRequest
                                 responseClass:(Class)responseClass
                       withrequestOperationType:(NSString *)requestOperationType // added to handle asynchronious responses;
                                showLoadingView:(NSNumber *)showLoadingView;
@end
