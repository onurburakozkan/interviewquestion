    //
//  BaseViewController.m
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import "BaseViewController.h"
#import "BaseResponse.h"
#import "Utility.h"
#import "AppDelegate.h"

@interface BaseViewController ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *responses;
@property (nonatomic) BOOL refreshNeeded;
@property (nonatomic) BOOL redisplayNeeded;
@property (nonatomic, weak) UITapGestureRecognizer *tapRecognizer;

@end

@implementation BaseViewController

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		self.redisplayNeeded = YES;
		self.responses = [[NSMutableDictionary alloc] init];
        _storeData = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void) awakeFromNib
{
	[super awakeFromNib];
	self.redisplayNeeded = YES;
	self.responses = [[NSMutableDictionary alloc] init];
    _storeData = [[NSMutableDictionary alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.navigationController.interactivePopGestureRecognizer.delegate = self;

}
-(void) viewDidLoad{
    
    [super viewDidLoad];
    self.title = @"";
    [self registerForKeyboardNotifications];
    
}
-(void) setNeedsRefreshData{

    NetworkHTTPRequestOperation *requestOperation = [self mainRequest];
    if (requestOperation == nil) {
        return;
    }
    
    if([requestOperation.showLoading isEqualToNumber:@1])
        [Utility addLoadingView];
    
    NSLog(@"\n\n-----------------------\nApp is waiting Synchronious BACKEND server Response...\n-----------------------\n\n");
    
    [self startRequest:requestOperation];


}

-(void)startRequest:(NetworkHTTPRequestOperation*)requestOperation{
    
    __weak NetworkHTTPRequestOperation *weakRequestOperation = requestOperation;
    
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [Utility removeLoadingView];
        
        self.data = responseObject;
        [self mainRequestDone:weakRequestOperation];

     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         
         if(error.code == -999){
             NSLog(@"%@ is canceled",operation.request.URL.absoluteString);
         }
         else{
             [self mainRequestWithOperation:weakRequestOperation WentWrong:error];
         }
         
         [Utility removeLoadingView];
         
     }
     ];
    
    [requestOperation start];

}

-(NetworkHTTPRequestOperation *) mainRequest
{
	return nil;
}
-(void)mainRequestDone:(NetworkHTTPRequestOperation *)request {
    
    [self.responses setValue:request.responseObject forKey:NSStringFromClass([request.responseObject class])];
    
}
-(void)mainRequestWithOperation:(NetworkHTTPRequestOperation *)operation WentWrong:(NSError *)error {
    
    if (error!=nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An Error Occured" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
}
-(UIView*)findFirstResponder:(UIView*)parentView {
    if (parentView == nil) {
        parentView = self.view;
    }
    if (parentView.isFirstResponder) {
        return parentView;
    }
    for (UIView *subView in parentView.subviews) {
        UIView *r = [self findFirstResponder:subView];
        if (r) return r;
    }
    return nil;
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return !([touch.view isKindOfClass:[UIControl class]]);
}
-(void)handleTap
{
    [[self findFirstResponder:nil] resignFirstResponder];
}
-(void)removeTapGesture {
    
    if (self.tapRecognizer) {
        [self.view removeGestureRecognizer:self.tapRecognizer];
        self.tapRecognizer = nil;
    }
    
}
-(void)setPreventsTapGesture:(BOOL)preventsTapGesture {
    _preventsTapGesture = preventsTapGesture;
    [self removeTapGesture];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Keyboard Settings
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    if (!self.preventsTapGesture && !self.tapRecognizer) {
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
        recognizer.delegate = self;
        [[self view] addGestureRecognizer:recognizer];
        self.tapRecognizer = recognizer;
        
    }
    

}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    [self removeTapGesture];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
@end
