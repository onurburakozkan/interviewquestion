//
//  NetworkHTTPRequestOperation.m
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import "NetworkHTTPRequestOperation.h"
#import "BaseResponse.h"
#import "AppDelegate.h"
#import "ErrorDefinition.h"
#import "NetworkHTTPClient.h"

static dispatch_queue_t json_request_operation_processing_queue() {
    static dispatch_queue_t af_json_request_operation_processing_queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        af_json_request_operation_processing_queue = dispatch_queue_create("com.metglobal.networking.json-request.processing", DISPATCH_QUEUE_CONCURRENT);
    });
    
    return af_json_request_operation_processing_queue;
}

@interface NetworkHTTPRequestOperation ()
@property (readwrite, nonatomic, strong) Class responseClass;
@property (readwrite, nonatomic, strong) id responseObject;
@property (readwrite, nonatomic, strong) NSError *JSONError;
@property (readwrite, nonatomic, strong) NSRecursiveLock *lock;

@end

@implementation NetworkHTTPRequestOperation
@dynamic lock;

+ (instancetype)BaseRequestOperationWithRequest:(NSMutableURLRequest *)urlRequest
                                  responseClass:(Class)responseClass
                       withrequestOperationType:(NSString *)requestOperationType
                                showLoadingView:(NSNumber *)showLoadingView{
    
    NetworkHTTPRequestOperation *requestOperation = [(NetworkHTTPRequestOperation *)[self alloc] initWithRequest:urlRequest];
    requestOperation.responseClass = responseClass;
    requestOperation.requestUrl =urlRequest;
    requestOperation.requestOperationType = requestOperationType;
    requestOperation.showLoading = showLoadingView;
    
    if(requestOperation){
        [[[NetworkHTTPClient requestQueueList] operationQueue] addOperation:requestOperation];

    }
    
    return requestOperation;
}

- (id)responseObject {
    [self.lock lock];
    if (!_responseObject && [self.responseData length] > 0 && [self isFinished] && !self.JSONError) {
        NSError *error = nil;
        
        // Workaround for behavior of Rails to return a single space for `head :ok` (a workaround for a bug in Safari), which is not interpreted as valid input by NSJSONSerialization.
        // See https://github.com/rails/rails/issues/1742
        if ([self.responseData length] == 0 || [self.responseString isEqualToString:@" "]) {
            self.responseObject = nil;
        } else {
            // Workaround for a bug in NSJSONSerialization when Unicode character escape codes are used instead of the actual character
            // See http://stackoverflow.com/a/12843465/157142
            NSString *jsonString = [[NSString alloc] initWithData:self.responseData  encoding:NSUTF8StringEncoding];
            if (jsonString == nil) {
                error = [NSError errorWithDomain:@"METLOBAL" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Error parsing response."}];
                self.responseObject = nil;
            } else {
                
                if(self.responseClass == nil){
                    NSDictionary *innerJSON = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
                    self.responseObject = innerJSON;
                }
                else{
                    NSError* err = nil;
                    self.responseObject = [[self.responseClass alloc] initWithString:jsonString error:&err];
                }
                
            }
        }
        
        self.JSONError = error;
    }
    [self.lock unlock];
    
    return _responseObject;
}

- (NSError *)error {
    if (_JSONError) {
        return _JSONError;
    } else {
        NSError *afError =[super error];
        NSError *redefinedError = nil;
        if (!(afError ==nil)) {
            redefinedError = [NSError errorWithDomain:@"AFNetworking" code:afError.code  userInfo:@{NSLocalizedDescriptionKey:[ErrorDefinition changeErrorDescriptionWithErrorCode:afError.code witfExistingErrorText:[afError.userInfo objectForKey:@"NSLocalizedDescription"]]}];
        }
        return redefinedError;
    }
}
#pragma mark - AFHTTPRequestOperation

+ (NSSet *)acceptableContentTypes {
    return [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", nil];
}

+ (BOOL)canProcessRequest:(NSURLRequest *)request {
    return [[[request URL] pathExtension] isEqualToString:@"json"] || [super canProcessRequest:request];
}

- (void)setCompletionBlockWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
    self.completionBlock = ^ {
        NSLog(@"\n\n*************************************************\n\nURL: %@\nHTTP Method: %@\nResponse Class: %@\n\n----------REQUEST JSON----------\n%@\n\n----------RESPONSE JSON----------\n\n%@\n\n*************************************************\n\n",self.request.URL,self.request.HTTPMethod,NSStringFromClass([self.responseObject class]),
                  [[NSString alloc] initWithData:self.request.HTTPBody  encoding:NSUTF8StringEncoding], self.responseString);
        if (self.error) {
            if (failure) {
                dispatch_async(self.failureCallbackQueue ?: dispatch_get_main_queue(), ^{
                    
                    failure(self, self.error);
                    
                });
            }
        } else {
            dispatch_async(json_request_operation_processing_queue(), ^{
                
                if (self.JSONError) {
                    if (failure) {
                        dispatch_async(self.failureCallbackQueue ?: dispatch_get_main_queue(), ^{
                            failure(self, self.error);
                        });
                    }
                } else {
                    if (success) {
                        dispatch_async(self.successCallbackQueue ?: dispatch_get_main_queue(), ^{
                            success(self, self.responseObject);
                        });
                    }
                    
                }
            });
        }
    };
#pragma clang diagnostic pop
}
-(BOOL)showLoadingView{
    return self.showLoadingView;
}

@end
