//
//  NetworkHTTPClient.m
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import "NetworkHTTPClient.h"
#import "NetworkHTTPRequestOperation.h"
#import "Utility.h"

#define APP_SRV @"https://api.booking.com/v3/interviews/"

@implementation NetworkHTTPClient

+ (NetworkHTTPClient *)sharedClient {
    static NetworkHTTPClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[NetworkHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:APP_SRV]];
    });
    
    return _sharedClient;
}
+ (NetworkHTTPClient *)requestQueueList{
    static NetworkHTTPClient *_requestQueueList = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _requestQueueList = [[NetworkHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@""]];
    });
    
    return _requestQueueList;
}
- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[NetworkHTTPRequestOperation class]];
    
    return self;
}

- (NSMutableURLRequest *)requestWithPath:(NSString *)path
                         parameters:(NSMutableDictionary *)parameters
                         httpmethod:(NSString *)httpMethod
{
    self.parameterEncoding = AFJSONParameterEncoding;
    self.stringEncoding = NSUTF8StringEncoding;
    
    [self setAuthorizationHeaderWithUsername:@"key_071dd21f192f601a" password:@"secret_3902d6cc2c8deda7fb805af7b07f8a18dc9f5925ee0992c338fd31b163688013"];
    
    NSMutableURLRequest *request = [super requestWithMethod:httpMethod path:path parameters:parameters];

	return request;
    
}
- (NSMutableURLRequest *)requestWithBaseRequest:(BaseRequest *)requestObject
{
    
    
    NSMutableDictionary* requestDict = [NSMutableDictionary dictionaryWithDictionary:[requestObject toDictionary]];
    [requestDict removeObjectForKey:@"requestUrl"];
    [requestDict removeObjectForKey:@"request_type"];
    
    return  [self requestWithPath:requestObject.requestUrl parameters:requestDict httpmethod:requestObject.request_type];
    
}

+ (void)cancelAllRequest:(Class)cancelledRequestClass{
    
    if(cancelledRequestClass ==nil){
        [[[NetworkHTTPClient requestQueueList] operationQueue] cancelAllOperations];
    }
    else{
        
        BaseRequest *baseRequest =[[cancelledRequestClass alloc] init];
        NSString *urlBeginsWith = [APP_SRV stringByAppendingString:baseRequest.requestUrl];
        
        for (NetworkHTTPRequestOperation *requestOperation in [[NetworkHTTPClient requestQueueList] operationQueue].operations) {

            if([[[requestOperation.request.URL absoluteString] substringToIndex:urlBeginsWith.length] isEqualToString:urlBeginsWith] ){
                [requestOperation cancel];
            }
            
        }
    }
    
}


@end
