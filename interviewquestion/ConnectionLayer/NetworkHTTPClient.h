//
//  NetworkHTTPClient.h
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import "AFHTTPClient.h"
#import "NetworkHTTPRequestOperation.h"
#import "BaseRequest.h"

@interface NetworkHTTPClient : AFHTTPClient

+ (NetworkHTTPClient *)sharedClient;
+ (NetworkHTTPClient *)requestQueueList;

+ (void)cancelAllRequest:(Class)cancelledClass;
- (NSMutableURLRequest *)requestWithPath:(NSString *)path
                              parameters:(NSMutableDictionary *)parameters
                              httpmethod:(NSString *)httpMethod;
- (NSMutableURLRequest *)requestWithBaseRequest:(BaseRequest *)requestObject;

@end
