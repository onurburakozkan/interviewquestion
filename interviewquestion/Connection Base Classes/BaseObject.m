//
//  BaseObject.m
//  Direkt
//
//  Created by Onur Burak Özkan on 04/01/2014.
//
//

#import "BaseObject.h"

@implementation BaseObject

-(id)valueForUndefinedKey:(NSString *)key {
    return nil;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
}
@end
