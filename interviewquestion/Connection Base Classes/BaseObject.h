//
//  BaseObject.h
//  Direkt
//
//  Created by Onur Burak Özkan on 04/01/2014.
//
//

#import <JSONModel.h>

@interface BaseObject : JSONModel <NSCoding>


@end
