//
//  BaseRequest.h
//  Direkt
//
//  Created by Onur Burak Özkan on 04/01/2014.
//
//

#import "BaseObject.h"

@interface BaseRequest : BaseObject

@property (nonatomic, strong) NSString *requestUrl;
@property (nonatomic, strong) NSString *request_type;//GET,POST,PUT

@end
