//
//  BaseResponse.m
//  Direkt
//
//  Created by Onur Burak Özkan on 04/01/2014.
//
//

#import "BaseResponse.h"

@implementation BaseResponse

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
    
}
+ (Class)non_field_errors_class
{
    return [NSString class];
}
+ (Class)error_messages_class
{
    return [NSString class];
}
@end
