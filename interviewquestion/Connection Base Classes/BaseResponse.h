//
//  BaseResponse.h
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import "BaseObject.h"

@interface BaseResponse : BaseObject <NSCoding>

@end
