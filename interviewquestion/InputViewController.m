//
//  InputViewController.m
//  interviewquestion
//
//  Created by Onur Burak Ozkan on 22/09/14.
//  Copyright (c) 2014 interview. All rights reserved.
//

#import "InputViewController.h"

@interface InputViewController ()

@end

@implementation InputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Input";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)searchButtonTouchUpInside:(UIButton *)sender {
    [self performSegueWithIdentifier:@"searchSegue" sender:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"searchSegue"]){
        AvailabilityViewController *destViewController = segue.destinationViewController;
        
        destViewController.storeData[@"longitude"] = self.longitudeTextField.text;
        destViewController.storeData[@"latitude"] = self.latitudeTextField.text;
    }
}


@end
