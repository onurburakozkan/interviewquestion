//
//  HotelViewModelByCityCell.m
//  OtelComPhone
//
//  Created by Onur Burak Ozkan on 26/05/14.
//  Copyright (c) 2014 Metglobal. All rights reserved.

#import "AvailabilityModelCell.h"

@implementation AvailabilityModelCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

-(void)setNeedsLayout
{
    [self setSelectionStyle:UITableViewCellSelectionStyleGray];
    
}

-(void)setModel:(AvailabilityModel *)model {
    [super setModel:model];
    
    if(model.objectId == nil){
            objectId.text = @"Id : No information";
    }
    else{
        objectId.text = [NSString stringWithFormat:@"Id : %@",model.objectId];
    }
    
    if(model.name == nil){
        name.text = @"Name : No information";
    }
    else{
        name.text = [NSString stringWithFormat:@"Name : %@",model.name];
    }

    if(model.price == nil){
        price.text = @"Price : No information";
    }
    else{
        price.text = [NSString stringWithFormat:@"Price : %@",model.price];
    }

    if(model.objectDescription == nil){
        objectDescriptionTextView.text = @"No Dsecription";
    }
    else{
        objectDescriptionTextView.text = [NSString stringWithFormat:@"Description :%@",model.objectDescription];
    }
    
    if(model.latitude == nil){
        latitude.text = @"Latitude :No information";
    }
    else{
        latitude.text = [NSString stringWithFormat:@"Latitude :%.4f",[model.latitude doubleValue]];
    }

    if(model.longitude == nil){
        longitude.text = @"Longitude :No information";
    }
    else{
        longitude.text = [NSString stringWithFormat:@"Longitude :%.4f",[model.longitude doubleValue]];
    }
    
    if(model.main == nil){
        main.text = @"Main :No information";
    }
    else{
        main.text = [NSString stringWithFormat:@"Main :%@",model.main];
    }
    
    if(model.stars == nil){
        stars.text = @"Stars :No information";
    }
    else{
        stars.text = [NSString stringWithFormat:@"Stars :%d",[model.stars intValue]];
    }

    if(model.reviews_score_word == nil){
        reviews_score_word.text = @"R_Score_Word :No information";
    }
    else{
    reviews_score_word.text = [NSString stringWithFormat:@"R_Score_Word :%@",model.reviews_score_word];
    }
    
    if(model.reviews_score == nil){
        reviews_score.text = @"R_Score :No information";
    }
    else{
    reviews_score.text = [NSString stringWithFormat:@"R_Score :%.1f",[model.reviews_score doubleValue]];
    }
    
    if(model.reviews_count == nil){
        reviews_count.text = @"R_Count :No information";
    }
    else{
        reviews_count.text = [NSString stringWithFormat:@"R_Count :%d",[model.reviews_count intValue]];
    }
    
}
+(int)heightOfTableCell{
    return 350;
}
@end
