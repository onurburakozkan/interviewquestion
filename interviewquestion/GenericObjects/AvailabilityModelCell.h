//
//  HotelViewModelByCityCell.h
//  OtelComPhone
//
//  Created by Onur Burak Ozkan on 26/05/14.
//  Copyright (c) 2014 Metglobal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelCell.h"
#import "AvailabilityModel.h"

@interface AvailabilityModelCell : ModelCell
{
    
    IBOutlet UILabel * objectId;
    IBOutlet UILabel * name;
    IBOutlet UILabel * price;
    IBOutlet UILabel * latitude;
    IBOutlet UILabel * longitude;
    IBOutlet UILabel * stars;
    IBOutlet UILabel * main;
    IBOutlet UILabel * reviews_count;
    IBOutlet UILabel * reviews_score;
    IBOutlet UILabel * reviews_score_word;
    IBOutlet UITextView *objectDescriptionTextView;
    
}

+(int)heightOfTableCell;

@end
