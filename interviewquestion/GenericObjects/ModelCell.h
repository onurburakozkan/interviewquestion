//
//  ModelCell.h
//  Direkt
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//
#import <UIKit/UIKit.h>


@interface UITableView (ModelCell)

-(id)dequeueReusableCellWithModel:(NSObject *)model;
-(id)dequeueReusableCellWithModel:(NSObject *)model nibName:(NSString*)nibName;

@end

@interface ModelCell : UITableViewCell

@property (nonatomic, strong) NSObject *model;

@end
