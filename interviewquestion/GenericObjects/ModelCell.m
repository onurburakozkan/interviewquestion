//
//  ModelCell.m
//
//  Created by Onur Burak Özkan on 03/01/2014.
//
//

#import "ModelCell.h"

@implementation UITableView (ModelCell)

-(id)dequeueReusableCellWithModel:(NSObject *)model {
    NSString *nibName = [NSString stringWithFormat:@"%@Cell", NSStringFromClass(model.class)];
    return [self dequeueReusableCellWithModel:model nibName:nibName];
}

-(id)dequeueReusableCellWithModel:(NSObject *)model nibName:(NSString *)nibName {
    ModelCell *cell;
    if([[NSBundle mainBundle] pathForResource:nibName ofType:@"nib"] != nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil] lastObject];
    } else {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NSStringCell" owner:nil options:nil] lastObject];
    }
    
    cell.model = model;
    return cell;
}

@end

@implementation ModelCell


@end
