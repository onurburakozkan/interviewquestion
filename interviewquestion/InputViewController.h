//
//  InputViewController.h
//  interviewquestion
//
//  Created by Onur Burak Ozkan on 22/09/14.
//  Copyright (c) 2014 interview. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvailabilityViewController.h"

@interface InputViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *longitudeTextField;
@property (strong, nonatomic) IBOutlet UITextField *latitudeTextField;

@end
