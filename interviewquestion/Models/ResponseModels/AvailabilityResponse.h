//
//  NewsListResponse.h
//  Digestnews
//
//  Created by Onur Burak Ozkan on 25/07/14.
//  Copyright (c) 2014 Senfoni. All rights reserved.
//

#import "BaseResponse.h"
#import "AvailabilityModel.h"

@interface AvailabilityResponse : BaseResponse

@property(nonatomic,retain) NSArray<AvailabilityModel>* interviews_availability;

@end
