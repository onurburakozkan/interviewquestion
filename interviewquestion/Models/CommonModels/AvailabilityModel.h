//
//  ImageViewModel.h
//  Digestnews
//
//  Created by Onur Burak Ozkan on 27/07/14.
//  Copyright (c) 2014 Senfoni. All rights reserved.
//

#import "BaseObject.h"
@protocol AvailabilityModel
@end

@interface AvailabilityModel : BaseObject

@property(nonatomic,retain) NSString<Optional> *name;
@property(nonatomic,retain) NSString<Optional> *price;
@property(nonatomic,retain) NSString<Optional> *objectId;
@property(nonatomic,retain) NSString<Optional> *objectDescription;
@property(nonatomic,retain) NSNumber<Optional> *latitude;
@property(nonatomic,retain) NSNumber<Optional> *longitude;
@property(nonatomic,retain) NSNumber<Optional> *stars;
@property(nonatomic,retain) NSString<Optional> *main;
@property(nonatomic,retain) NSNumber<Optional> *reviews_count;
@property(nonatomic,retain) NSNumber<Optional> *reviews_score;
@property(nonatomic,retain) NSString<Optional> *reviews_score_word;

@end
