//
//  NewsListRequest.m
//  Digestnews
//
//  Created by Onur Burak Ozkan on 25/07/14.
//  Copyright (c) 2014 Senfoni. All rights reserved.
//

#import "AvailabilityRequest.h"

@implementation AvailabilityRequest
-(id)init {
    if (self = [super init]) {
        self.requestUrl = @"availability";
        self.request_type = @"GET";
    }
    return self;
}
@end
