//
//  NewsListRequest.h
//  Digestnews
//
//  Created by Onur Burak Ozkan on 25/07/14.
//  Copyright (c) 2014 Senfoni. All rights reserved.
//

#import "BaseRequest.h"

@interface AvailabilityRequest : BaseRequest

@property(nonatomic,retain) NSNumber *longitude;
@property(nonatomic,retain) NSNumber *latitude;

@end
