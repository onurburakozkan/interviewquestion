//
//  ProgressView.m
//  Hotelstore
//
//  Created by Onur Burak Ozkan on 18/03/14.
//  Copyright (c) 2014 Onur Burak Ozkan. All rights reserved.
//

#import "ProgressView.h"
#define ANIMATING_LOADING_VIEW 1000

@implementation ProgressView

static ProgressView *_sharedProgressView = nil;

+ (ProgressView *)sharedProgressView{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedProgressView = [[ProgressView alloc] initWithFrame:[[[UIApplication sharedApplication] delegate] window].bounds];
        _sharedProgressView.removeFromSuperViewOnHide = YES;
        _sharedProgressView.tag = ANIMATING_LOADING_VIEW;
    });
    
    return _sharedProgressView;
    
}

- (void)show{
    
    if ([[[[UIApplication sharedApplication] delegate] window] viewWithTag:ANIMATING_LOADING_VIEW] == nil) {
        
        [[[[UIApplication sharedApplication] delegate] window] addSubview:_sharedProgressView];
        [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:_sharedProgressView];
        [_sharedProgressView show:TRUE];
        
    }
    
}
- (void)dismiss{
    
    [[[[[UIApplication sharedApplication] delegate] window] viewWithTag:ANIMATING_LOADING_VIEW] removeFromSuperview];
    
}

@end
