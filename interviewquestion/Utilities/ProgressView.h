//
//  ProgressView.h
//  Hotelstore
//
//  Created by Onur Burak Ozkan on 18/03/14.
//  Copyright (c) 2014 Onur Burak Ozkan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ProgressView : MBProgressHUD

+ (ProgressView *)sharedProgressView;

- (void)show;
- (void)dismiss;

@end
