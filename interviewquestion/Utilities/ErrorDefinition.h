//
//  ErrorDefinition.h
//  Direkt
//
//  Created by Onur Burak Ozkan on 7/17/13.
//
//

#import <Foundation/Foundation.h>

@interface ErrorDefinition : NSObject

+(NSString*)changeErrorDescriptionWithErrorCode:(NSInteger)errorCode witfExistingErrorText:(NSString*)errorText;

@end
