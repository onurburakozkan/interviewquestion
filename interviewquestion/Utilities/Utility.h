//
//  Utility.h
//  Direkt
//
//  Created by Onur Burak Ozkan on 5/8/13.
//
//

#import <Foundation/Foundation.h>
#import "ProgressView.h"

@interface Utility : NSObject

+(void)addLoadingView;
+(void)removeLoadingView;

@end
