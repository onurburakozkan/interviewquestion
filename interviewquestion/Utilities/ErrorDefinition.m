//
//  ErrorDefinition.m
//  Direkt
//
//  Created by Onur Burak Ozkan on 7/17/13.
//
//

#import "ErrorDefinition.h"

@implementation ErrorDefinition

+(NSString*)changeErrorDescriptionWithErrorCode:(NSInteger)errorCode witfExistingErrorText:(NSString*)errorText{
    switch ((int)errorCode) {
            
        case -998:
            return @"ErrorUnknown.";
        case -999:
            return @"ErrorCancelled.";
        case -1000:
            return @"Error Bad URL";
        case -1001:
            return @"Request Timeout";
        case -1002:
            return @"ErrorUnsupportedURL";
        case -1003:
            return @"Server can not be found.";
        case -1004:
            return @"Server can not be reached.";
        case -1005:
            return @"ErrorNetworkConnectionLost.";
        case -1006:
            return @"ErrorDNSLookupFailed.";
        case -1007:
            return @"ErrorHTTPTooManyRedirects.";
        case -1008:
            return @"ErrorResourceUnavailable.";
        case -1009:
            return @"There is no internet connection";
        case -1010:
            return @"ErrorRedirectToNonExistentLocation.";
        case -1011:
            return @"Bad server response.";
        case -1012:
            return @"ErrorUserCancelledAuthentication.";
        case -1013:
            return @"ErrorUserAuthenticationRequired.";
        case -1014:
            return @"ErrorZeroByteResource.";
        case -1015:
            return @"ErrorCannotDecodeRawData.";
        case -1016:
            return @"ErrorCannotDecodeContentData.";
        case -1017:
            return @"ErrorCannotParseResponse.";
        case -1018:
            return @"ErrorInternationalRoamingOff.";
        case -1019:
            return @"ErrorCallIsActive.";
        case -1020:
            return @"ErrorDataNotAllowed.";
        case -1021:
            return @"ErrorRequestBodyStreamExhausted.";
        case -1100:
            return @"ErrorFileDoesNotExist.";
        case -1101:
            return @"ErrorFileIsDirectory.";
        case -1102:
            return @"ErrorNoPermissionsToReadFile.";
        case -1103:
            return @"ErrorDataLengthExceedsMaximum.";
            
        default:
            return errorText;
    }
}

@end