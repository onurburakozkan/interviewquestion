//
//  Utility.m
//  Direkt
//
//  Created by Onur Burak Ozkan on 5/8/13.
//
//

#import "Utility.h"

@implementation Utility

+(void)addLoadingView{
    
    [[ProgressView sharedProgressView] show];
    
}
+(void)removeLoadingView{
    
    [[ProgressView sharedProgressView] dismiss];
    
}
@end
