//
//  ViewController.m
//  interviewquestion
//
//  Created by Onur Burak Ozkan on 22/09/14.
//  Copyright (c) 2014 interview. All rights reserved.
//

#import "AvailabilityViewController.h"

@interface AvailabilityViewController ()

@end

@implementation AvailabilityViewController
#pragma mark - Request-Response handlers -
-(NetworkHTTPRequestOperation *)mainRequest {
    if([self.requestType isEqualToString:@"GetAvailabilities"])
    {
        AvailabilityRequest *model = [[AvailabilityRequest alloc]init];
        model.longitude = self.storeData[@"longitude"];
        model.latitude = self.storeData[@"latitude"];
        
        NSMutableURLRequest *request = [[NetworkHTTPClient sharedClient] requestWithBaseRequest:model];
        return [NetworkHTTPRequestOperation BaseRequestOperationWithRequest:request responseClass:[AvailabilityResponse class] withrequestOperationType:@"GetAvailabilities" showLoadingView:@1];
    }
    return nil;
}


-(void)mainRequestDone:(NetworkHTTPRequestOperation *)request{
    [super mainRequestDone:request];
    
    if([request.requestOperationType isEqualToString:@"GetAvailabilities"])
    {
        if([((AvailabilityResponse*)self.data).interviews_availability count]>0){
               [_availabilityTableView reloadData];
        }
        else{
            UILabel* noResultFoundLabel =[[UILabel alloc] initWithFrame:self.view.frame];
            noResultFoundLabel.backgroundColor = [UIColor redColor];
            noResultFoundLabel.textColor = [UIColor whiteColor];
            noResultFoundLabel.text = @"No Result found";
            noResultFoundLabel.textAlignment = NSTextAlignmentCenter;
            [self.view addSubview:noResultFoundLabel];
        
        }

    }
}
-(void)mainRequestWithOperation:(NetworkHTTPRequestOperation *)operation WentWrong:(NSError *)error{
    [super mainRequestWithOperation:operation WentWrong:error];
}
#pragma mark View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Results";
    
    self.requestType = @"GetAvailabilities";
    [self setNeedsRefreshData];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Tableview  Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [((AvailabilityResponse*)self.data).interviews_availability count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [AvailabilityModelCell heightOfTableCell];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AvailabilityModelCell *availabilityViewCell;
    availabilityViewCell =[tableView dequeueReusableCellWithModel:[((AvailabilityResponse*)self.data).interviews_availability objectAtIndex:indexPath.row]];
    return availabilityViewCell;
}

@end
